import express from "express";
import { cipher, decipher } from "../vigenere.js";

export const homeRouter = express.Router()

homeRouter.get('/', (req,res) => {
    res.render('home', { title: 'Hey', message: 'Hello there!'})
})

homeRouter.post('/cipher', (req,res) => {
    try {
        const ciphered_string = cipher(req.body.input, req.body.key)
        res.status(200).send(ciphered_string)
    } catch (error) {
        res.status(500).send("Error occured")
    }
})

homeRouter.post('/decipher', (req,res) => {
    try {
        const deciphered_string = decipher(req.body.input, req.body.key)
        res.status(200).send(deciphered_string)
    } catch (error) {
        res.status(500).send("Error occured")
    }
})