const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

export const cipher = (string, key) => {
    const chars = string.split('')
    let encrypted_string = ""
    let encrypted_char
    let key_index = 0

    for(const char of chars) {
        encrypted_char = cipherLetter(char,key[key_index].toUpperCase())
        
        if (isInCharset(encrypted_char.toUpperCase())) {
            key_index++
            key_index= key_index % key.length
        }

        encrypted_string += encrypted_char
    }

    return encrypted_string
}

export const decipher = (string, key) => {
    const chars = string.split('')
    let decoded_string = ""
    let decoded_char
    let key_index = 0

    for(const char of chars) {
        decoded_char = decipherLetter(char,key[key_index].toUpperCase())
        
        if (isInCharset(decoded_char.toUpperCase())) {
            key_index++
            key_index= key_index % key.length
        }

        decoded_string += decoded_char
    }

    return decoded_string
}

const cipherLetter = (char,key) => {
    let enc_char
    let ref_char

    ref_char = char.toUpperCase()

    if(!isInCharset(ref_char)){
        enc_char = char
    }else {
        enc_char = charset[(charset.indexOf(ref_char) + charset.indexOf(key)) % 26]
    }

    return isUpperCase(char) ? enc_char : enc_char.toLowerCase()
}

const decipherLetter = (char,key) => {
    let decoded_char
    let ref_char

    ref_char = char.toUpperCase()

    if(!isInCharset(ref_char)){
        decoded_char = char
    }else {
        decoded_char = charset[(charset.indexOf(ref_char) - charset.indexOf(key) + 26) % 26]
    }

    return isUpperCase(char) ? decoded_char : decoded_char.toLowerCase()
}

const isInCharset = (char) => {
    return charset.indexOf(char) !== -1
}

const isUpperCase = (char) => {
    if (char === char.toUpperCase()) {
        return true
    }
    return false
}