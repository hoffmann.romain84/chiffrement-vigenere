import assert from 'assert'
import {cipher, decipher} from '../src/lib/vigenere.js'

// CIPHER

it('should cipher classic string with a standard key', () => {
    assert.equal(cipher('ABCDEFGHIJKLMNOPQRSTUVWXYZ','CLE'), 'CMGFPJISMLVPOYSRBVUEYXHBAK');
  });

  it('should cipher special string with a standard key', () => {
    assert.equal(cipher('AB!dkj(kh§','CLE'), 'CM!hmu(oj§');
  });

  it('should cipher multicase string with a standard key', () => {
    assert.equal(cipher('abCdEFgHiJKLMNoPqRsTuvWyZ','CLE'), 'cmGfPJiSmLVPOYsRbVuEyxHcB');
  });

  it('should cipher string with A starting key', () => {
    assert.equal(cipher('AAA AAA AAA AAA AAA AAA','ABCD'), 'ABC DAB CDA BCD ABC DAB');
  });

  it('should cipher string with A multicase key', () => {
    assert.equal(cipher('AAA AAA AAA AAA AAA AAA','abCd'), 'ABC DAB CDA BCD ABC DAB');
  });

  // DECIPHER


it('should decipher classic string with a standard key', () => {
  assert.equal(decipher('CMGFPJISMLVPOYSRBVUEYXHBAK','CLE'), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
});

it('should decipher special string with a standard key', () => {
  assert.equal(decipher('CM!hmu(oj§','CLE'), 'AB!dkj(kh§');
});

it('should decipher multicase string with a standard key', () => {
  assert.equal(decipher('cmGfPJiSmLVPOYsRbVuEyxHcB','CLE'), 'abCdEFgHiJKLMNoPqRsTuvWyZ');
});

it('should decipher string with A starting key', () => {
  assert.equal(decipher('ABC DAB CDA BCD ABC DAB','ABCD'), 'AAA AAA AAA AAA AAA AAA');
});

it('should decipher string with A multicase key', () => {
  assert.equal(decipher('ABC DAB CDA BCD ABC DAB','AbCd'), 'AAA AAA AAA AAA AAA AAA');
});