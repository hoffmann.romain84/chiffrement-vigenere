window.addEventListener('load', () => {
    const submit = document.querySelector('#submit')
    const input = document.querySelector('#user-input')
    const key = document.querySelector('#user-key')
    let output = document.querySelector('#user-output')
    let mode = document.querySelector('#mode')
    let url = "http://localhost:3000/cipher"

    mode.addEventListener('change',() => {
        submit.innerHTML = mode.value
        url = mode.value === 'Cipher' ? "http://localhost:3000/cipher" :  "http://localhost:3000/decipher" 
    })

    

    submit.addEventListener('click',async () => {

        const test = await fetch(url, {
            method: 'POST',
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify({
        input: input.value,
        key: key.value,
        mode: mode
    })
    })
        output.value = await test.text()
    })
})