import express from 'express'
import { homeRouter } from './src/lib/route/home.mjs.js'
import bodyParser from 'body-parser'

const app = express()

const PORT = 3000

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json())

app.set('view engine', 'pug');
app.set('views','./public/views');

app.use(homeRouter)

app.listen(PORT, () => {
    console.log(`Server listening on http://localhost:${PORT}`)
})